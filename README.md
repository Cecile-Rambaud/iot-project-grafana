# IoT - OpenIT


## Pré-requis 

- Nodejs : https://nodejs.org/en
- Grafana : https://grafana.com/grafana/download?platform=windows
- InfluxDB : https://dl.influxdata.com/influxdb/releases/influxdb2-2.5.1-windows-amd64.zip
- Middleware - API : https://expressjs.com/fr/starter/installing.html
- Capteurs – projet tp-iot : archive zip


## Installation

### InfluxDB :
- Se placer à la racine du dossier C:\...\ influxdb2_windows_amd64 et lancer la commande
influxd.exe
- Se connecter à l’url localhost:8086
- Créer un API Token via l’onglet « load data » qui permettra à votre middleware de
communiquer avec votre base de donnée via API

### Grafana :
- après installation se rendre sur localhost:3000/login et se connecter avec admin/admin
- créer la connexion entre grafana et influxdb via data source
(https://docs.influxdata.com/influxdb/cloud/tools/grafana/?t=InfluxQL)

### Capteurs :
Configurez vos constantes sous la partie configuration de l'humidité.js et de la température.js

Vous pouvez laisser les constantes telles qu'elles sont, sauf pour le ENDPOINT. Vous devez remplacer la valeur vide par votre point de terminaison d'API.

Pour démarrer les capteurs, exécutez les commandes suivantes :
``` bash
npm i
npm run sensors
```

***


